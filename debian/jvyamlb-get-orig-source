#!/bin/sh

# This script is used to download the upstream source for jvyamlb and
# generate it into an orig source tarball for Debian.

# Common variables used to ease maintenance of this script
JVYAMLB_TARBALL="jvyamlb-src-0.2.5.tar.gz"
JVYAMLB_TARBALL_CHECKSUM="700036c76ee344b6d82444195e1595547b2f012c"
JVYAMLB_VERSION="0.2.5"

USAGE="\n\
This script is used to generate the orig tarball used in building\n\
Debian packages for jvyamlb-$JVYAMLB_VERSION.\n\
Usage: jvyamlb-get-orig-source [OPTION]\n\
\n\
 -h, --help                 Display this help message.\n\
 --remove-upstream-tarball  Remove the upstream source tarball.\n"

while [ "$#" -gt "0" ]
do
    case "$1" in
        --remove-upstream-tarball)
            REMOVE_UPSTREAM_TARBALL=1
            shift
            ;;
        -h|--help|*)
            echo "${USAGE}"
            exit 1
            ;;
    esac
done

make_current_tarball() {
    # Download the tarball if it's not available in the current directory
    [ -f $JVYAMLB_TARBALL ] || \
        wget -c http://jvyamlb.googlecode.com/files/$JVYAMLB_TARBALL

    # Verify the checksum
    COMPUTED_CHECKSUM=`sha1sum $JVYAMLB_TARBALL | cut -d ' ' -f 1`
    if [ $JVYAMLB_TARBALL_CHECKSUM != $COMPUTED_CHECKSUM ] ; then
        echo "Checksum verification failed. Checksum was $COMPUTED_CHECKSUM
    Expected checksum $JVYAMLB_TARBALL_CHECKSUM."
        exit 1
    else
        echo "Checksum verified. Checksum is $COMPUTED_CHECKSUM."
    fi

    echo "Copying upstream tarball with different name and running jh_repack."
    cp -f $JVYAMLB_TARBALL jvyamlb_${JVYAMLB_VERSION}.orig.tar.gz
    jh_repack --upstream-version ${JVYAMLB_VERSION} jvyamlb_${JVYAMLB_VERSION}.orig.tar.gz
    if [ $REMOVE_UPSTREAM_TARBALL ]; then
        echo -n "Removing upstream tarball..."
        rm $JVYAMLB_TARBALL
        echo "done."
    fi
}

make_current_tarball
